/*
global require, process, module
*/

// Global methods
var joinPath = require('path').join;

// Global Classes
var NodePackageService = require('./NodePackageService');

var DIR = {
  CWD: process.cwd(),
  CONVERSION: 'conversion'
};

var TASK = {
  DEFAULT: 'default',
  CONVERT: 'convert'
};

module.exports = {
  DIR: DIR,
  TASK: TASK
};
