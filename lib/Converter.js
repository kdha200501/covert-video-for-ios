/*
global require, module
*/

// Global Classes
var LogService = require('./LogService');

// Global Methods
var fileSuffix = require('path').extname;

var Converter = function() {// closure
  'use strict';

  // private static class
  var singleton = null;
  var Converter_ = function(config) {
    return function(nodePackageList) {// closure

      // private variables and methods
      var fileList = [];
      var regVideoFile = /(rmvb)|(mp4)|(mkv)|(m4v)|(mpg)|(mpeg)|(avi)|(wmv)|(flv)|(3gp)|(vob)|(mts)|(ts)/i;

      var convert = function(filePath) {
        if( !regVideoFile.test(filePath) ) {
          return;
        }
        //endIf file is not a video file

        LogService.warn.call('converter', 'filePath:', filePath);
        LogService.warn.call('converter', 'suffix:', fileSuffix(filePath));
      };

      // public methods
      return {
        convert: convert
      };

    }();// endClosure
  };

  // public singleton
  return {
    getInstance: function() {
      if(singleton === null) {
        singleton = new Converter_();
      }
      return singleton;
    }
  };
}();// endClosure

module.exports = Converter;
