/*
global require, module
*/

// Global Methods
var registerTask = require('gulp').task;
var chainTask = require('gulp').series;
var convergeTask = require('gulp').parallel;
var watch = require('gulp').watch;
var gulp = require('gulp');
var safeAccess = require('safe-access');
var noop = require('node-noop').noop;
var exit = require('gulp-exit');
var directoryExists = require('fs').existsSync;
var ls = require('fs').readdirSync;
var joinPath = require('path').join;
var map = require('map-stream');

// Global Classes
var NodePackageService = require('./NodePackageService');
var LogService = require('./LogService');
var VinylService = require('./VinylService');
var Queue = require('queue');
var Converter = require('./Converter');

// Global Constants
var TASK = require('./GulpConst').TASK;

// Global Variables
var DIR_CONVERSION = require('./GulpArgv').DIR_CONVERSION;
var DIR_WATCH = require('./GulpArgv').DIR_WATCH;

/*
    note:
        gulp.src() works with paths relative to process.cwd()
        require() works with paths relative to this file's __dirname
*/

var GulpTaskService = function() {// closure
  'use strict';

  // private static variables
  var nodePackageService = NodePackageService.getInstance();
  var vinylService = VinylService.getInstance();

  var gulpTaskList = [{

    name: 'validateWatchFolder',
    method: function(next, logInfo, logError) {
      if(!DIR_WATCH) {
        logError('watch directory input not specified');
        exit();
      }
      else if( !directoryExists(DIR_WATCH) ) {
        logError('the specified watch directory does not exist:', DIR_WATCH);
        exit();
      }
      else {
        next();
      }
    }

  }, {

    name: 'conversionMkdirP',
    method: function(next, logInfo, logError) {
      if( directoryExists(DIR_CONVERSION) ) {
        next();
      }
      else {
        return nodePackageService.mkdirP(DIR_CONVERSION, function(err) {
          if(err === null) {
            logInfo('making directory:', DIR_CONVERSION);
            next();
          }
          else {
            logError(err);
            exit();
          }
        });
      }
    }

  }, {

    name: 'convert',
    method: function() {
      var src = gulp.src( joinPath(DIR_WATCH, '**') );
      return src.pipe( map(vinylService.convert) );
    }

  }, {

    name: 'watchDirectory',
    method: function(next, logInfo) {
      //var glob = new Glob( joinPath(DIR_WATCH, '**', '*.mp4') , {nocase: true});
      //logInfo( glob.toString() )

      var converter = Converter.getInstance();

      watch(joinPath(DIR_WATCH, '**'), {ignoreInitial: true})
        .on('add', function(filePath) {
          converter.convert(filePath);
        })
        .on('change', function(filePath) {
          converter.convert(filePath);
        });

    }

  }];

  // private static class
  var singleton = null;
  var GulpTaskService_ = function(config) {
    return function(gulpTaskList) {// closure

      // private variables and methods
      var buildService = function(service, gulpTask) {
        var logInfo = function() {// prepended info log with task name
          LogService.info.apply(gulpTask.name, arguments);
        };
        var logError = function() {// prepended error log with task name
          LogService.error.apply(gulpTask.name, arguments);
        };
        var method = function(next) {
          if(gulpTask.name !== 'noop') {
            logInfo('Started');
          }
          return gulpTask.method(next, logInfo, logError);
        };
        registerTask(gulpTask.name, method);
        service[gulpTask.name] = method;
        return service;
      };

      // public methods
      return gulpTaskList.reduce(buildService, {});

    }(config.gulpTaskList || []);// endClosure
  };

  // public singleton
  return {
    getInstance: function() {
      if(singleton === null) {
        singleton = new GulpTaskService_({gulpTaskList: gulpTaskList});
      }
      return singleton;
    }
  };
}();// endClosure

module.exports = GulpTaskService;
