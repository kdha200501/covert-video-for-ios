/*
global require, module, Buffer, JSON
*/

// Global Methods
var noop = require('node-noop').noop;
var exit = require('gulp-exit');

// Global Classes
var NodePackageService = require('./NodePackageService');
var LogService = require('./LogService');
var Converter = require('./Converter');
var Queue = require('queue');

// Global Variables

var VinylService = function() {// closure
  'use strict';

  // private static variables
  var nodePackageService = NodePackageService.getInstance();

  var vinylMethodList = [{

    name: 'convert',
    method: function(vinyl, next){
      var converter = Converter.getInstance();
      converter.convert(vinyl.path);
      next();
    }

  }];

  // private static class
  var VinylService_ = function(config) {
    return function(vinylMethodList) {// closure

      // private variables and methods
      var buildService = function(service, vinylMethod) {
        service[vinylMethod.name] = vinylMethod.method;
        return service;
      };

      // public methods
      return vinylMethodList.reduce(buildService, {});

    }(config.vinylMethodList || []);// endClosure
  };

  // public singleton
  var singleton = null;
  return {
    getInstance: function() {
      if(singleton === null) {
        singleton = new VinylService_({vinylMethodList: vinylMethodList});
      }
      return singleton;
    }
  };
}();// endClosure

module.exports = VinylService;
