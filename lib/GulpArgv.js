/*
global require, module
*/

// Global Methods
var joinPath = require('path').join;

// Global Classes
var NodePackageService = require('./NodePackageService');

// Global Constants
var TASK = require('./GulpConst').TASK;
var DIR = require('./GulpConst').DIR;

// Global Variables
var nodePackageService = NodePackageService.getInstance();
var $1 = nodePackageService.getArgs()._[0];// bash $1

var DIR_CWD = nodePackageService.getArgs().workingDirectory || DIR.CWD;

module.exports = {
  $1: $1 === undefined ? TASK.CONVERT : $1,// default gulp to the GRAB_TORRENT task
  DIR_CWD: DIR_CWD,
  DIR_CONVERSION: joinPath(DIR_CWD, DIR.CONVERSION),
  DIR_WATCH: nodePackageService.getArgs().watchDirectory
};
